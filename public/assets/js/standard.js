var phaserManager = {
  loadResours: function(game){
    this.game = game;
    game.load.image('fondo', '/assets/images/fondo2.jpg?'+(Math.random()*1));
    game.load.image('NuestrosValoresWhite', '/assets/images/nuestrosvalores-white.png');
    game.load.image('fullscreen','/assets/images/expand.png');
    game.load.audio('click','/assets/audio/click.mp3');
    game.load.audio('wrong','/assets/audio/wrong.mp3');
    game.load.audio('correct','/assets/audio/correct.mp3');
    game.load.audio('backaudio','/assets/audio/background.mp3');
  },
  paleta :{
    blue: "#1D70B7",
    green: "#76B72B",
    purple: "#662482",
    yellow: "#FABA09",
    white: "#fff",
    black: "#263238",
    red : "#E31837"
  },
  shuffle: function (array) {
    var currentIndex = array.length, temporaryValue, randomIndex;

    // Mientras queden elementos a mezclar...
    while (0 !== currentIndex) {

      // Seleccionar un elemento sin mezclar...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex -= 1;

      // E intercambiarlo con el elemento actual
      temporaryValue = array[currentIndex];
      array[currentIndex] = array[randomIndex];
      array[randomIndex] = temporaryValue;
    }
    return array;
  },
  saveScore: function(aciertos,tiempo,juegoName){
    data = {"aciertos":aciertos,"tiempo":tiempo,"juegoName":juegoName};
  //  console.log(data);
    $.ajax({
      url:"/saveScore",
      type:"post",
      data:data
    }).done(function(response){
      console.log(response);
    });
  },
  getBestTime(nombre,funcion){
    $.ajax({
      url:"/getBestTime/"+nombre,
      type:"get",
    }).done(funcion);
  }
}
