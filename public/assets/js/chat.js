$(function(){
  client.emit("id","");
  client.on("id",function(id){
    message.myId = id;
    message.getAllMessages();
  });
  client.on("presence:state",function(state){
    console.log(state);
    var userList = state.map(function(user){
      return {devices:user.payload.length,active:"active",nombre:user.payload[0].meta.nombre+" "+user.payload[0].meta.apellidos,username:user.payload[0].meta.username,id:user.id}
    })
    message.usersActive = userList;
    message.createListUsers(message.usersActive,message.allUsers);
    // createListUsers(usersActive,allUsers);
    //document.getElementById('list-users').innerHTML = userList.join('');
  });
  client.on("disconnect",function(){
    document.getElementById('list-users').innerHTML = "Good bye!";
  });
  client.on("message",function(mensaje){
    console.log(message);
    message.createListMessages([mensaje]);
    $("#message").val("");
  });
  $("#btn-send").click(function(event){
    if($("#message").val()!=""){
      mensaje = $("#message").val();
      client.emit("message",mensaje);
    }
  });
  message.getUsers();
});
