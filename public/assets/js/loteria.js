
var juego = {
  cards:[],

  __init_game:function(){
    client.emit("begin",localStorage.getItem('clave'));
    var game = new Phaser.Game(860, 490, Phaser.CANVAS, 'app-game', { preload: preload, create: create});
    var x_card =490;
    var counter = 0;
    function preload() {
      console.log(game);
      	/*
      	|--------------------------------------------------------------------------
      	| Como crar un nuevo proyecto
      	|--------------------------------------------------------------------------
      	|
      	| En esta parte cargamos todas los recursos: somidos imagenees fondos
      	| para crear los
      	| videojuegos que vayamos ha desarrollar.
      	| El metod phaserManager.loadResours(); nos carga todos los recursos
        | necesarios para crear la estructura estandar del juego
        | esto es un archivo de ejemplo de como se crearía un videojuegos nuevo
        | siguendo la estructura estandar.
        | Para desarrollar un nuevo videojuegos  tendríamos que crear
        | un archivo js con el nombre del juego que vamos a crear por ejemplo
        | memorama.js y dentro de este archivo copiar todo este codigo que se
        | se encuentra en este archivo.
        | para acceder a este juego que acabamos de crear solo tendríasmos que
        | poner en la url: /game/nobre_de_mi_archivo_js.js que acabamos de crear
        | y listo ya tendríamos un nuevo proyecto listo para desarrollar
        | Nota: para acceder a la ruta del juego necesitamos estar previamente
        | logeados en el sistema.
      	*/
        phaserManager.loadResours(game);
        game.load.image("card","/assets/images/card.png");
        loadCardsLoteria();//load all cars use in the lotería
    }
    function loadCardsLoteria(){
      for (var i = 0; i < juego.cards.length; i++) {
        game.load.image(juego.cards[i].nombre,juego.cards[i].src)
      }
    }

    function create() {
      createdAll(game);
      addCards();
    }
    function addCards(){
      for (var i = 0; i < juego.cards.length; i++) {
        var card = game.add.button(260+(i*.3),280,"card",cardClick,this);
        card.scale.set(.14);
        card.index = i;
      }
    }
    function cardClick(card){
      click.play();
      counter++;
      card.destroy();
      var carta = game.add.sprite(x_card,280,juego.cards[card.index].nombre);
      x_card += 3;
      carta.width  = card.width;
      carta.height = card.height;
      client.emit("cart",juego.cards[card.index]);
      if(counter==juego.cards.length){
        //alert("El juego a terminado- "+counter);
        client.emit("finish",localStorage.getItem('clave'));
      }
    }
  },
  toggleGame:function(){
    $("#cont-main").hide("fast");
    $(".game").show("fast");
    juego.__init_game();
  },
  getCards:function(){
     console.log("getCartas");
      $.ajax({
        url:"/getCard",
        type:"POST"
      }).done(function(response){
        console.log(response);
        if(response.length>0){
          juego.cards = response;
          phaserManager.shuffle(juego.cards); // revolvemos todas las cartas
          juego.toggleGame();//iniciamos juego
        }else{
          toastr.info("No hay cartas suficientes para jugar con esta version e lotería intente con otra version.");
          $("#re-clave").hide("fast");
          $("#re-reg").show("slow");
        }
      })
  }
}
