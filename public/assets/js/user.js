$(function(){
  var $frmR = $("#frm-register");
  $("#li-init").trigger("click");
  $("#btn-register").click(function(){
    $(this).addClass("disabled");
    $(this).text("registrando....");
    $(this).prop("disabled","disabled");
    if($("#email-").val()=="" || $("email-r").val()=="" || $("#password-r").val()==""){
      toastr.info("Favor de llenar todos los campos");
    }else{
      data = {
        email: $("#email-").val(),
        password: $("#password-r").val(),
        cpassword:$("#cpassword").val(),
        username : $("#email-r").val()
      }
      toastr.info("Enviando correo y registrando....");
      $.ajax({
        url:"/register",
        method:"POST",
        data: data
      }).done(function(response){
        if(response.status=="200"){
          toastr.success(response.message+', ahora puedes iniciar sesion',"Registro completado.");
          $("#btn-close").trigger("click");
          $frmR[0].reset();
          setTimeout(function(){
              document.location = "/";
          },3500)

          //Document.getElementById('frm-register').reset();
        }else if(response.status==401){
          toastr.info("Fallaron las valiadciones")
          console.log(response);
          $.each(response.data,function(object,value){
            toastr.info(value.message);
          });
        }
      }).fail(function(error){
        console.error(error);
      });
    }
  });
  $("#btn-login").click(function(event){
    data = {
      username: $("#email").val(),
      password:  $("#password").val()
    };
    $.ajax({
      url:"/login",
      method:"POST",
      data:data
    }).done(function(response){
      if(response.status=="200"){
        toastr.success(response.message,"Nuestros Valores:");
        $("#btn-close").trigger("click");
        document.location = "/reparto";
      }else if(response.status=="D-106"){
        toastr.info("email o contraseña incorrectos");
      }else{
        toastr.info("email o contraseña incorrectos");
      }
    }).fail(function(error){
      toastr.info("email o contraseña incorrectos");
    });
  });
});
