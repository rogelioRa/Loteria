var componentes = {
  listUser:(props)=>{
    let comp =
      '<a class="list-group-item ln-h"><span class="initial '+props.active+'"></span> '+props.nombre+'('+props.username+') <i class="fa fa-tablet"></i> '+props.devices+'</a>';
    return comp;
  },
  itemUser:(props)=>{
    let comp = '<div class="item '+props.active+' text-center"><i data-toggle="modal" data-user="'+props.username+'" data-target="#md-tablero" class="fa item fa-user-circle fa-4x"></i><p>'+props.username+'</p></div>';
    return comp;
  },
  message:(props)=>{
    let comp =
      '<li class="message '+props.position+' appeared"><div class="avatar"><p>'+props.letter+'</p></div><div class="text_wrapper"><div class="text">'+props.message+'</div></div></li>';
    return comp;
  },
  acordion:(props)=>{
    let comp = '<div class="accordion" id="accordion-'+props.index+'" role="tablist" aria-multiselectable="true"></div>';
    return comp;
  },
  itemsAcordion:(props)=>{
    let comp =
      '<div class="card my-card-acordion">'+
          '<div class="card-header" role="tab" id="heading-'+props.index+''+props.indexParent+'">'+
              '<a data-toggle="collapse" data-parent="#accordion-'+props.indexParent+'"   href="#collapse-'+props.index+''+props.indexParent+'" aria-expanded="true" aria-controls="collapse-'+props.index+''+props.indexParent+'">'+
                  '<h5 class="mb-0">'+
                    'Regla '+props.index+' <i class="fa fa-angle-down rotate-icon"></i>'+
                  '</h5>'+
              '</a>'+
          '</div>'+
          '<div id="collapse-'+props.index+''+props.indexParent+'" class="collapse '+props.show+'" role="tabpanel" aria-labelledby="heading-'+props.index+''+props.indexParent+'">'+
              '<div class="card-block">'+
                  props.text+
              '</div>'+
          '</div>'+
      '</div>';
      console.log(comp);
    return comp;
  }
}
