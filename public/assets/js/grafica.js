$(function(){
object = {
  data:null,
  getData : function(){
    $.ajax({
      url:"/getEstadisticas",
      type:"GET"
    }).done(function(response){
      console.log(response);
      object.data  = response;
      object.createGrafica();
    });
  },
  getCategorias:function(){
    var categorias = [];
    for(var i = 0;i<object.data.length;i++){
      categorias.push(object.data[i].username);
    }
    return categorias;
  },
  getDataSerie: function(){
    var data = [];
    for(var i =0;i<object.data.length;i++){
      $.each(object.data[i],function(index,value){
        data.push(value);
      })
    }
    return data;
  },
  createGrafica: function(){
    Highcharts.chart('grafica', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Puntos obtenidos por usuario'
        },
        subtitle: {
            text: 'Lotería GeRo'
        },
        xAxis: {
            categories: ["jugados","ganados","puntos obtenidos"]//object.getCategorias()
        },
        yAxis: {
            title: {
                text: 'Puntos obtenidos'
            }
        },
        plotOptions: {
            line: {
                dataLabels: {
                    enabled: true
                },
                enableMouseTracking: false
            }
        },
        series: [{
            name:"usuarios",
            data: object.getDataSerie()
        }]
    });
  }
}
object.getData();
});
