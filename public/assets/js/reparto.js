$(function(){
  //  juego.getCards();
  var object ={
    clave:"",
    tableros:[],
    sendRequest : function(ruta,data,callback){
      $.ajax({
        url:ruta,
        type:"POST",
        data:data
      }).done(callback);
    },
    getTableroFromUsername : function(username){
      var tablero = {};
      $.each(object.tableros,function(clave,valor){
        if(valor.username == username){
          tablero =  valor;
        }
      });
      return tablero;
    },
    buildCart : function(tablero){
        $("#row-tablero img").each(function(index,img){
          if(tablero.created_cart!=undefined)
            $(img).attr("src",tablero.created_cart[index].src);
        });
    },
    getData : function(){
      return {
        nombre   : $("#nombre").val(),
        version  : $("#version").val(),
        limit    : $("#limit").val(),
        patrones : $("#patrones").val()
      }
    }
  }
  client.on("tablero",function(tablero){
    object.tableros.push(tablero);
  });
  client.on("presence:state",function(state){
    console.log(state);
    var userList = state.map(function(user){
      return {devices:user.payload.length,active:"",email:user.payload[0].meta.email,username:user.payload[0].meta.username,id:user.id}
    })
    partida.createList(userList)
  });

  client.on('join',function(join){
    console.log(join);
    if(localStorage.getItem('clave') == join.clave){
      partida.list.push({active:"active",username:join.username});
      partida.createListConnected();
    }
  });

  client.on('won',function(message){
    /*{username:"user",patron:"vertical",partida:"room_clave"}*/
    console.log(message);
  });

  $("#btn-create-part").click(function(){
    $("#re-welcome").hide("fast");
    $("#re-reg").show("slow");
  });

  $("#btn-save-partida").click(function(){
    if($("#nombre").val()=="" || $("#patrones").val().length==0){
      toastr.info("Favor de llenar todos los datos");
    }else{
      object.sendRequest("/save-partida",object.getData(),function(response){
        if(response.status==200){
          toastr.success(response.message);
          $("#re-reg").hide("fast");
          $("#re-clave").show("slow");
          object.clave = response.data.clave;
          $("#txt-clave").text(object.clave);
          localStorage.setItem("clave",object.clave);
        }else if(response.status==401){
          toastr.info("Fallaron las valiadciones")
          $.each(response.data,function(object,value){
            toastr.info(value.message);
          });
        }
      });
    }
  });
  $("body").on("click",".item>i",function(){
     var username = $(this).data("user");
     var tablero  = object.getTableroFromUsername(username);
     object.buildCart(tablero);
  });
  $("#btn-begin-game").click(function(){
    juego.getCards();
  });
});
