/*WebFontConfig = {

    //  'active' means all requested fonts have finished loading
    //  We set a 1 second delay before calling 'createText'.
    //  For some reason if we don't the browser cannot render the text the first time it's created.
    active: function() { game.time.events.add(Phaser.Timer.SECOND, createText, this); },

    //  The Google Fonts we want to load (specify as many as you like in the array)
    google: {
      families: ['Finger Paint']
    }

};
*/
var seguridad,etica,ambiente,respeto;
var correct;
var wrong;
var click
var buttonFullScreen;
var game;

function createdAll(games){
  game = games;
  game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
	// add background to game
	//var sprite = game.add.tileSprite(0, 0,game.width,game.height,'fondo');
  game.stage.backgroundColor = "rgb(29,112,183)";
  game.add.tileSprite(0,0,game.width,game.height,'fondo');
  //add icon for value

  //var icon = game.add.sprite(game.width-790,-65,'seguridad');
  //icon.scale.set(1.5);
  var backaudio = game.add.audio("backaudio");
  backaudio.play();
  backaudio.volume = .3;
  //add full screen
  buttonFullScreen = game.add.button(game.width-85,game.height-85,'fullscreen',actionOnClick, this, 2, 1, 0);
  buttonFullScreen.scale.set(.4);
   var logo = game.add.sprite(375,43,"NuestrosValoresWhite");
   logo.scale.set(.6);
   //logo.scale.set(.8);
  // console.log(logo.width,logo.height);
  // sets card in view
  createMarco();
  loadAudios();
}

function loadAudios(){
  click   = game.add.audio("click");
  wrong   = game.add.audio("wrong");
  correct = game.add.audio("correct");
}
function createMarco(){
  //  Create a nice and complex graphics object
   var graphics = game.add.graphics(0, 0);
   graphics.lineStyle(15, 0xe73d48);
    graphics.moveTo(12,15);
    graphics.lineTo(game.width-2,15);// up full
    graphics.moveTo(game.width-5,20);
    graphics.lineTo(game.width-5,game.height-5);  // right full
    graphics.moveTo(20,20);
    graphics.lineTo(20, game.height); // left full
    graphics.moveTo(12,game.height-5);
    graphics.lineTo(game.width-2, game.height-10); // bottom full
}




function actionOnClick () {
  if(game.scale.isFullScreen){
    game.scale.stopFullScreen();
  }else{
    game.scale.startFullScreen();
  }
}
