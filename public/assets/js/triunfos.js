$(function(){
  var $cards = $(".my-cards .card");
  var object = {
    patron:[],
    getPatron:function(){
      object.patron = [];
      $cards.each(function(index,obj){
        if($(obj).hasClass("active")){
          object.patron.push(index);
        }
      });
      return object.patron;
    },
    sendRequest : function(ruta,data,response){
      var patron = object.getPatron();
      if(patron.length==0){
        toastr.info("Seleccione algunas casilla para realizar un patron de triunfo");
      }else{
        $.ajax({
          url : ruta,
          method:"POST",
          data: data
        }).done(function(response){
          if(response.status == 200){
            toastr.success(response.message);
            object.reload();
          }else if(response.status == 401){
            toastr.info("Algunos datos no fueron llenados de forma correcta");
          }else{
            toastr.error("Error desconosido");
          }
        });
      }
    },
    getData:function(){
      var patron = object.getPatron();
      return {
        patron      : patron.toString(),
        nombre      : $("#nombre").val(),
        descripcion : $("#descripcion").val(),
        puntos      : $("#puntos").val()
      }
    },
    reload : function(){
      setTimeout(function(){window.location.reload();},1000);
    },
    setPatron(array){
      $cards.removeClass("active");
      console.log(array);
      object.patron = [];
      for(var i =0;i<array.length;i++){
          $($cards[array[i]]).addClass("active");
      }
      object.patron = array;
    },
    togglemenu : function(){
      $("#card-triunfo").toggle("slow");
      $("#table-triunfo").toggle("fast");
    }
  }

  $cards.click(function(){
    if($(this).hasClass("active")){
      $(this).removeClass("active");
    }else{
        $(this).addClass("active");
    }
  });

  $("#btn-registrar").click(function(){
     const data = object.getData();
     object.sendRequest("/registrar-patron",data);
  });

  $("#btn-update").click(function(){
    const id = $(this).data("id");
    const data = object.getData();
    data.id = id;
    object.sendRequest('/update-patron',data);
  });

  $("#btn-delete").click(function(){
    const id = $(this).data("id");
    const data = {};
    data.id = id;
    swal({
      title: '¿Está usted segúro?',
      text: "Desea eliminar este patron de triunfo!",
      type: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
      }).then(function () {
        object.sendRequest("/delete-patron",data);
        swal(
          'Deleted!',
          'Your file has been deleted.',
          'success'
        )
    });
    console.log(data);
  })

  $("#btn-ver").click(function(){
    object.togglemenu();
  });

  $("#btn-return").click(function(){
    $("#btn-update").hide();
    $("#btn-delete").hide();
    $("#btns-save").show();
    object.togglemenu();
    $("#nombre").val('');
    $("#descripcion").val('');
    object.setPatron([]);
  });

  $(".btn-modify").click(function(){
    $("#btn-update").show();
    $("#btn-delete").show();
    $("#btns-save").hide();
    let id = JSON.stringify($(this).data("id"));
    $("#btn-update").data("id",id);
    $("#btn-delete").data("id",id);
    $("#nombre").val($(this).parent().siblings(".td-nombre").text());
    $("#descripcion").val($(this).parent().siblings(".td-descripcion").text());
    $("#puntos").val($(this).parent().siblings(".td-puntos").text());
    var patron = eval("["+$(this).parent().siblings(".td-patron").text()+"]");
    object.setPatron(patron);
    object.togglemenu();
  });

});
