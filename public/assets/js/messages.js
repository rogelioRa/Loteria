var  message = {
  allUsers:[],
  usersActive:[],
  myId:0,
  getUsers:function(){
    $.ajax({
      url:"/getUsers",
      type:"POST"
    }).done(function(users){
      message.allUsers = users;
      message.createListUsers(message.usersActive,message.allUsers);
    })
  },
  createListMessages:function(messages){
    var list = messages.map(function(mensaje){
      var position;
      var letter = message.getLetterUser(mensaje.user_id);
      position = (mensaje.user_id==message.myId)?'right':'left';
      return componentes.message({position:position,message:mensaje.message,letter:letter});
    });
    $("#my-messages-list").append($(list.join("")));
    var hijos =parseInt($("#my-messages-list").children().length);
    scrollTop = $("#my-messages-list").height()*hijos;
    $('#my-messages-list').animate({scrollTop:scrollTop});
  },
  createListUsers:function(userActive,allusers){
    for(var i=0; i<userActive.length;i++){
      for(var j=0;j<allusers.length;j++){
        if(allusers[j].id == userActive[i].id){
          allusers[j].devices = userActive[i].devices;
          allusers[j].active  = "active";
        }
      }
    }
    message.createList(allusers);
  },
  createList: function(items){
    var list= items.map(function(item){
      return componentes.listUser(item);
    });
    document.getElementById('list-users').innerHTML = list.join('');
  },
  getLetterUser:function(id){
    for(var i =0;i<message.allUsers.length;i++){
      if(id == message.allUsers[i].id){
        return message.allUsers[i].username.substring(0,1).toUpperCase();
      }
    }
  },
  getAllMessages(){
    $.ajax({
      url:"/messages",
      type:"GET"
    }).done(function(messages){
      message.createListMessages(messages);
    });
  }
}
