$(function(){
  var card = {
    getData:function(){
      var formdata = new FormData($("frm-carta")[0]);
      formdata.append("imagen",$("#imagen")[0].files[0]);
      formdata.append('nombre',$("#nombre").val());
      formdata.append('frase',$("#frase").val());
      formdata.append('version',$("#version").val());
      console.log($("#imagen")[0].files);
      return formdata;
    }
  }
  $("#btn-registrar").click(function(event){
    $.ajax({
      url:"/registrar-carta",
      method:"POST",
      processData: false,
      contentType: false,
      cache:false,
      data: card.getData()
    }).done(function(response){
      if(response.status == 200){
        toastr.success(response.message);
        $("#frm-carta")[0].reset();
      }else if(response.status==401){
        toastr.info("Algunos datos no fueron llenados de forma correcta");
      }else{
        toastr.error("Error desconosido");
      }
    }).fail(function(error){
      console.log(error);
    });
  });
});
