'use strict'
const User = use('App/Model/User')
const Partida = use('App/Model/Partida')
const Patron = use('App/Model/Patron')
const TriunfoDesglose = use('App/Model/TriunfoDesglose')
class PartidaController {

  constructor (socket, request,presence) {
    this.socket   = socket
    this.request  = request
    this.presence = presence
    if(socket.currentUser){
      presence.track(socket,socket.currentUser.id,{id:socket.currentUser.id,username:socket.currentUser.username,email:socket.currentUser.email})
    }
    console.log("channel partida SOCKET WAS CONECTED",socket.id)
  }

  * onPartida(clave){
      const partida = yield this.request.session.get("partida");
      if( partida.room_clave == clave ){
        console.log("You are welcome to the lotería");
      }else{
        console.log("La clave ingresada es incorrecta");
      }
  }

  * onJoin(join){
    const user = yield User.query()
                  .where('username','=',join.username)
    this.socket.exceptMe().emit('join',join)
  }

  * onCart(cart){
    this.socket.exceptMe().emit("cart",cart)
    //console.log("the cart is recived",cart)
  }

  * onCreate(tablero){
    console.log("llego el tablero");
    this.socket.emit("tablero",tablero);
    //console.log(tablero);
  }


  disconnected(socket){
    console.log("socket disconnected id %",socket.id);
  }

  * onBegin(clave){
    console.log(clave)
    const partida = yield Partida.query()
                    .where('room_clave','=',clave)
    this.socket.toEveryone().emit("begin",partida)
  }

  * onPatron(message){
    console.log(message)
    this.socket.toEveryone().emit("patron",message)
  }

  * onWon(message){
    try{
      const user = yield User.query().where('username','=',message.username)
      const partida = yield Partida.query().where('room_clave','=',message.room_clave)
      const patron = yield Patron.query().where('nombre','=',message.patron_nombre)
      const triunfo = new TriunfoDesglose()
      triunfo.users_id = user[0].id
      triunfo.partida_id = partida[0].id
      triunfo.patrones_id = patron[0].id
      yield triunfo.save()
    }catch(e){
      console.log(e);
    }
    this.socket.toEveryone().emit("won",{username:message.username,patron:message.patron_nombre,partida:message.room_clave})
  }

  * onFinish(message){
    this.socket.toEveryone().emit("finish",message)
  }
}

module.exports = PartidaController
