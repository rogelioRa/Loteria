'use strict'
const Message 			= use('App/Model/message')
class ChatController {

  constructor (socket, request, presence) {
    this.socket   = socket
    this.request  = request
    this.presence = presence
    presence.track(socket,socket.currentUser.id,{nombre:socket.currentUser.nombre,apellidos:socket.currentUser.apellidos,id:socket.currentUser.id,username:socket.currentUser.username})
    console.log("SOCKET CONECTED",socket.id)
  }
  * onMessage(message){
    console.log("recive de message",message);
    var message =  new Message({message:message,user_id:this.socket.currentUser.id})
    yield message.save(function (err) {
      if (err) {
        console.log(err);
      } else {
        console.log('meow');
      }
    });
    this.socket.toEveryone().emit("message",message);
  }
  onId(data){
    console.log("recived request")
    this.socket.toMe().emit("id",this.socket.currentUser.id);
  }
  disconnected(socket){
    console.log("socket disconnected id %",socket.id);
  }
}

module.exports = ChatController
