'use strict'
const Lucid = use('Lucid')

class Patron extends Lucid {
  static get table(){
    return 'patrones';
  }
}

module.exports = Patron
