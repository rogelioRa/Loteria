'use strict'

const Lucid = use('Lucid')

class Token extends Lucid {

  user () {
    return this.belongsTo('App/Model/User')
  }

  static timestamps (){
    return null
  }

}

module.exports = Token
