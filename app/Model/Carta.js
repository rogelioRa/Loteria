'use strict'
const Lucid = use('Lucid')

class Carta extends Lucid {
  static get table(){
    return 'cartas'
  }
}

module.exports = Carta
