'use strict'
const Lucid = use('Lucid')

class UsersPartida extends Lucid {
  static get table(){
    return "users_partidas";
  }
}

module.exports = UsersPartida
