'use strict'
const Lucid = use('Lucid')

class Version extends Lucid {
  static get table(){
    return 'versiones';
  }
}

module.exports = Version
