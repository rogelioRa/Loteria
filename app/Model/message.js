'use strict'

/**
*  @var Mongoose mongoose
*/
const mongoose = use('Mongoose')
const ObjectId = mongoose.Schema.ObjectId
let messageSchema = mongoose.Schema({
    id: ObjectId,
    message: String,
    date: {
        type: Date,
        default: Date.now
    },
    url_image: {type:String,default:null},
    user_id: Number,
});

module.exports = mongoose.model('message', messageSchema)
