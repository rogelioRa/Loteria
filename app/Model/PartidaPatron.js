'use strict'
const Lucid = use('Lucid')

class PartidaPatron extends Lucid {
  static get table(){
    return "partidas_patrones";
  }
  static get deleteTimestamp () {
    return null
  }
  static get updateTimestamp () {
      return null
  }
  static get createTimestamp () {
      return null
  }

}

module.exports = PartidaPatron
