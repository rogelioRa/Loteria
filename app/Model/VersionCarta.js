'use strict'
const Lucid = use('Lucid')

class VersionCarta extends Lucid {
  static get table(){
    return "versiones_cartas";
  }
  static get deleteTimestamp () {
    return null
  }
  static get updateTimestamp () {
      return null
  }
  static get createTimestamp () {
      return null
  }

}

module.exports = VersionCarta
