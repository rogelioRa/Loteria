'use strict'
const Lucid = use('Lucid')

class TriunfoDesglose extends Lucid {
  static get table(){
    return "triunfos_desgloce";
  }
}

module.exports = TriunfoDesglose
