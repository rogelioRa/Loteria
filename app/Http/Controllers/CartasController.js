'use strict'


const DB 				   = use('Database')
const Version      = use('App/Model/Version')
const Carta        = use('App/Model/Carta')
const Patron       = use('App/Model/Patron')
const VersionCarta = use('App/Model/VersionCarta')
const Validator    = use('Validator')
const Helpers      = use('Helpers')
const Ws    		   = use("Ws")

class CartasController {

  * register(request,response){
    const card  =  request.file('imagen',{ maxSize: '2mb',
      allowedExtensions: ['jpg', 'png', 'jpeg']
    })
    const rules       = {nombre:'required',frase:'required',version:'required'};
		const Data        = request.all();
    const validation  = yield Validator.validate(Data,rules)
    //return response.send(card)
    if(validation.fails()){
      response.json({"message":'fallaron las validaciones',status:401,data:validation.messages()})
      return
    }
    const fileName = `${new Date().getTime()}.${card.extension()}`
    yield card.move(Helpers.publicPath('assets/images/cartas/'), fileName)
    if (!card.moved()) {
      response.badRequest(card.errors())
      return
    }
    var carta    = new Carta()
    carta.nombre = request.input("nombre")
    carta.frase  = request.input("frase")
    carta.src    = 'assets/images/cartas/'+fileName;
    yield carta.save();
    const versionCarta        = new VersionCarta()
    versionCarta.cartas_id    = carta.id
    versionCarta.versiones_id = request.input("version")
    yield versionCarta.save()
    response.json({status:200,message:'La carta se ha dado de lata exitosamente'})
  }
  * registerPatron(request,response){
    const rules = {nombre:'required',descripcion:'required',patron:'required',puntos:"required"};
		const Data  = request.all();
    const validation  = yield Validator.validate(Data,rules)
    //return response.send(card)
    if(validation.fails()){
      response.json({"message":'fallaron las validaciones',status:401,data:validation.messages()})
      return
    }

    const patron       = new Patron()
    patron.nombre      = request.input("nombre")
    patron.descripcion = request.input("descripcion")
    patron.match       = request.input("patron")
    patron.puntos      = request.input("puntos")
    yield patron.save()
    response.json({status:200,message:'El patron se ha guardado exitosamente'})
  }
  * updatePatron(request,response){
    const rules = {nombre:'required',descripcion:'required',patron:'required',id:'required',puntos:"required"};
		const Data  = request.all();
    const validation  = yield Validator.validate(Data,rules)
    //return response.send(card)
    if(validation.fails()){
      response.json({"message":'fallaron las validaciones',status:401,data:validation.messages()})
      return
    }
    const id = request.input("id")
    console.log(id,typeof(id));
    const patron       = yield Patron.find(id)
    console.log(patron);
    patron.nombre      = request.input("nombre")
    patron.descripcion = request.input("descripcion")
    patron.match       = request.input("patron")
    patron.puntos      = request.input("puntos")
    yield patron.save()
    response.json({status:200,message:'El patron se ha actualizado exitosamente'})
  }
  * deletePatron(request,response){
    const id      = request.input("id")
    const patron  = yield Patron.find(id)
    if(patron){
      yield patron.delete()
      response.json({status:200,message:'El patron se ha actualizado exitosamente'})
      return
    }
    response.json({status:401,message:'El patron que desea eliminar no fue encontrado'})
  }
  * get(request,response){
    const partida    = yield request.session.get("partida")
    const version_id = partida.versiones_id
    const cards      = yield Carta.query()
                            .join("versiones_cartas",'versiones_cartas.cartas_id','=','cartas.id')
                            .join("versiones",'versiones.id','=','versiones_cartas.versiones_id')
                            .where("versiones.id","=",version_id)
                            .select("cartas.*")
    return response.json(cards)
  }
 * getCartas(request,response){
    console.log("Se realizo la petición");
   try{
     let cartas = yield Carta.all();
     response.json(cartas);
   }catch(e){
     console.log(e);
   }

 }
}
// select c.* from cartas  c join
// 			versiones_cartas vc on vc.cartas_id = c.id
//             join versiones v on v.id = vc.versiones_id
//             where v.id =2;
module.exports = CartasController
