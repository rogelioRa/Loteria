'use strict'


const DB 		    = use('Database')
const User          = use('App/Model/User')
const Carta         = use('App/Model/Carta')
const Patron        = use('App/Model/Patron')
const PartidaPatron = use('App/Model/PartidaPatron')
const Partida       = use('App/Model/Partida')
const VersionCarta  = use('App/Model/VersionCarta')
const Validator     = use('Validator')
const UsersPartida  = use('App/Model/UsersPartida')

class PartidaController {

  getRandomInt(max,min){
    return (Math.floor(Math.random() * (max - min)) + min);
  }

  * getEstadisticas(request,response){
    const estadisticas = yield User.query().join("triunfos_desgloce","triunfos_desgloce.users_id","=","users.id")
        .join("users_partidas","users.id","=","users_partidas.user_id")
        .join("patrones","patrones.id","triunfos_desgloce.patrones_id")
        .where("users.id","=",request.currentUser.id)
        .groupBy("users.id")
        .select(DB.raw("count(users_partidas.id) jugados"),DB.raw("count(triunfos_desgloce.id) ganados"),DB.raw("sum(patrones.puntos) puntos"))
    return response.json(estadisticas)
    // select count(up.id) jugados,count(td.id) ganados,sum(p.puntos) puntos from users_partidas up join users u
		// 	on up.user_id = u.id
    //         join triunfos_desgloce td on td.users_id = u.id
    //         join patrones p on p.id = td.patrones_id
    //         group by u.id;
  }

  * save(request,response){
    const rules       = {nombre:'required',limit:'required',version:'required',patrones:"required"};
		const Data        = request.all();
    const validation  = yield Validator.validate(Data,rules)
    //return response.send(card)
    if(validation.fails()){
      response.json({"message":'fallaron las validaciones',status:401,data:validation.messages()})
      return
    }
    const patrones       = request.input("patrones")
    const partida        = new Partida()
    partida.nombre       = request.input("nombre")
    partida.limite       = request.input("limit")
    partida.versiones_id = request.input("version")
    partida.user_id      = request.currentUser.id
    partida.room_clave   = this.getRandomInt(100000,900000)
    yield partida.save()
    for (var i = 0; i < patrones.length; i++) {
      const partidaPatron       = new PartidaPatron();
      partidaPatron.partida_id  = partida.id
      partidaPatron.patrones_id = patrones[i]
      yield partidaPatron.save();
    }
    const patrons = yield Partida.query()
                                  .join("partidas_patrones",'partidas_patrones.partida_id','partidas.id')
                                  .join("patrones",'partidas_patrones.patrones_id','patrones.id')
                                  .select("patrones.*")
                                  .where("partidas.id",'=',partida.id)

    const part = {id:partida.id,nombre:partida.nombre,limite:partida.limite,user_id:partida.user_id,versiones_id:partida.versiones_id,room_clave:partida.room_clave,patrones:patrons}
    yield request.session.put('partida', part)
    return response.json({message:"La partida se ha creado exitosamente ahora se pueden unir a la partida tus amigos.",status:200,data:{clave:partida.room_clave}})
  }

  * join(request, response){
    const partida = yield Partida
                    .query()
                    .where('partidas.room_clave','=',request.input('room_clave'))
    if(partida[0].accesible == 1){
      const user = yield User
                 .query()
                 .where('users.username','=',request.input('username'))
      const patrones = yield Partida.query()
                                    .join("partidas_patrones",'partidas_patrones.partida_id','partidas.id')
                                    .join("patrones",'partidas_patrones.patrones_id','patrones.id')
                                    .select("patrones.*")
                                    .where("partidas.id",'=',partida[0].id)
      console.log(partida[0].id)
      const userPartida = new UsersPartida()
      userPartida.user_id = user[0].id
      userPartida.partida_id = partida[0].id
      yield userPartida.save()
      const total  = yield DB.from('users_partidas').count('id as id').where('partida_id','=',partida[0].id)
      if(total[0].id == partida[0].limit){
        const updatePartida = Partida.find(partida[0].id)
        updatePartida.accesible = 0;
        yield updatePartida.save();
      }
      return response.json({status:200,partida:partida,patrones:patrones})
    }else{
      return response.json({status:201})
    }

  }

}

module.exports = PartidaController
