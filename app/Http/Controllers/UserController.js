'use strict'

const DB 				= use('Database')
const User 			= use('App/Model/User')
const Version   = use('App/Model/Version')
const Patron    = use('App/Model/Patron')
const Message  	= use('App/Model/message')
const Encryption = use('Encryption')
const Validator = use('Validator')
const Ws    		= use("Ws")
const Mail		  = use('Mail')
const Env       = use('Env')
class UserController {

	* login(request,response){

        const username = request.input('username')
	    const password = request.input('password')
	    const login		 = yield request.auth.attempt(username, password)
	    if (login) {
	      response.json({status:200,message:'Logged In Successfully'})
	      return
	    }
	    response.json({status:201,message:'Invalid credentails'})
	}
    * logout(request,response){
				const userLog = request.currentUser;
				if(!userLog){
					response.send("Log out");
					return
				}
				const usersSocket  = Ws.channel("chat").presence.pull(userLog.id,()=>true);
				usersSocket.forEach((socket)=>{
					socket.socket.disconnect();
				})
        yield request.auth.logout()
        return response.redirect('/')
    }
	* register(request,response){
		const rules = {email:'unique:users|required',username:'unique:users|required',password:'required'};
		const userData  = request.all();
        const validation  = yield Validator.validate(userData,rules)
        if(validation.fails()){
        	response.json({"message":'fallaron las validaciones',status:401,data:validation.messages()})
        	return
        }else{
        	const user     = new User()
					user.email     = request.input('email')
        	user.username  = request.input('username')
					user.password  = request.input('password')
					user.token     = Encryption.encrypt(request.input("email"))
        	yield user.save()
					console.log(user.token)
					let   ruta = Env.get("HOST")
					const port = Env.get("PORT")
					ruta = "http://"+ruta+":"+port+"/activar/"+user.token
					try {
						yield Mail.send('emails.welcome',{username:user.username,email:user.email,ruta:ruta}, (message) => {
				      message.to(user.email, user.username)
				      message.from('awesome@adonisjs.com')
				      message.subject('Welcome to the Lotería\'s World')
				    })
					} catch (e) {
						console.log(e)
					}
				   //yield user.delete();
	        response.json({message:"Usuario registrado con exito, se te ha mandado un correo de confirmación.",status:200,data:user})
	        return
        }
	}
	* showChat(request,response){
		return yield response.sendView("chat")
	}
	* getUsers(request,response){
		const users = yield DB.select("id","username",
																	DB.raw("concat('','') as active"),
																	DB.raw("concat('','0') as devices"),
																	DB.raw("concat(nombre,' ',apellidos) as nombre")).from("users");
		return response.json(users);
	}
	* getMessages(request,response){
		const messages = yield Message.find();

		// messages.forEach((message)=>{
		// 	if(message.user_id == request.currentUser.id){
		// 		message.mine = true;
		// 	}else{
		// 		message.mine = false;
		// 	}
		// });
		response.json(messages)
	}
	* getViewReparto(request,response){
		let versiones = yield Version.all()
		let patrones  = yield Patron.all()
		let   ruta = Env.get("HOST")
		const port = Env.get("PORT")
		ruta = "http://"+ruta+":"+port
		return yield response.sendView("reparto",{versiones: versiones.toJSON(),patrones:patrones.toJSON(),host:ruta})
	}
	* getViewTriunfos(request,response){
		const patrones = yield Patron.all()
		console.log(JSON.stringify(patrones))
		return yield response.sendView("admin-triunfo",{patrones:patrones.toJSON()})
	}
	* getViewCartas(request,response){
		let versiones = yield Version.all();
		return yield response.sendView("admin-cartas",{versiones: versiones.toJSON()})
	}
	* getViewEstadisticas(request,response){
		return yield response.sendView("grafica")
	}
	* activar(request,response){
		const token = request.param("token")
		const user  = yield User.findBy('token',token)
		if(user){
			user.active = 1
			yield user.save()
			return yield response.sendView("accountActive",{username:user.username})
			response.json({message:"El usuario ha sido activado exitosamente ahora puedes iniciar sesion.",status:200,data:user})
		}
		return response.json({status:401,message:"El usuario que desea activar no ha sido encontrado"})
	}
}

module.exports = UserController
