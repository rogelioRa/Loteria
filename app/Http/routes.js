'use strict'

/*
|--------------------------------------------------------------------------
| Router
|--------------------------------------------------------------------------
|
| AdonisJs Router helps you in defining urls and their actions. It supports
| all major HTTP conventions to keep your routes file descriptive and
| clean.
|
| @example
| Route.get('/user', 'UserController.index')
| Route.post('/user', 'UserController.store')
| Route.resource('user', 'UserController')
*/

const Route = use('Route')

Route.on('/').render('welcome')
Route.post('/login','UserController.login')
Route.post('/register','UserController.register')
Route.get('/logout','UserController.logout')
Route.get('/chat','UserController.showChat').middleware("auth")
Route.post("/getUsers",'UserController.getUsers').middleware("auth")
Route.get("/reparto",'UserController.getViewReparto').middleware("auth")
Route.get("/admin-cartas",'UserController.getViewCartas').middleware("auth")
Route.get("/admin-triunfo",'UserController.getViewTriunfos').middleware("auth")
Route.get("/admin-grafica",'UserController.getViewEstadisticas').middleware("auth")
Route.get("/getEstadisticas",'PartidaController.getEstadisticas').middleware("auth")
Route.post("/getCard",'CartasController.get').middleware("auth")
Route.post("/registrar-carta",'CartasController.register').middleware("auth")
Route.post("/update-patron",'CartasController.updatePatron').middleware("auth")
Route.post("/delete-patron",'CartasController.deletePatron').middleware("auth")
Route.post("/save-partida",'PartidaController.save').middleware("auth")
Route.post("/registrar-patron",'CartasController.registerPatron')
Route.post("getCartas","CartasController.getCartas")
Route.post("/unir-partida","PartidaController.join")

Route.get("/forceLogin",function *(request,response){
  try {
    yield request.auth.loginViaId(request.input("id"))
    response.send("Loggin success")
  } catch (e) {
    yield request.with({error: e.message}).flash()
    response.redirect('back')
  }
})
Route.get("/activar/:token","UserController.activar")
// Route.get("/activar",function *(request,response){
//   return yield response.sendView("accountActive")
// })
